// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "hardhat/console.sol";
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Burnable.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Burnable.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Strings.sol";

contract NFT is ERC721, ERC721Enumerable, Ownable {
    using Strings for uint;

    event Shipped(address owner, uint indexed tokenId);

    ERC20Burnable fungibleContract;
    string tokenBaseUri;
    uint artworkCount;
    uint public maxSupply;
    mapping (uint => bool) shippedStatus;

    constructor(
        string memory _name,
        string memory _symbol,
        address _fungibleContract,
        string memory _tokenBaseUri,
        uint _maxSupply,
        uint _artworkCount
    ) ERC721(_name, _symbol) {
        fungibleContract = ERC20Burnable(_fungibleContract);
        tokenBaseUri = _tokenBaseUri;
        maxSupply = _maxSupply;
        artworkCount = _artworkCount;
    }

    function tokenURI(uint tokenId) public view virtual override returns (string memory) {
        // require(_exists(tokenId), "ERC721Metadata: URI query for nonexistent token"); -- we need the URI for the "next token" in the Claim UI
        require(tokenId < maxSupply, "ERC721Metadata: URI query for nonexistent token");

        // we only have X artworks - so we modulo the tokenId with X, and append '.json' (or '-shipped.json')
        uint256 artworkNumber = tokenId % artworkCount;

        return string(abi.encodePacked(
            _baseURI(),
            artworkNumber.toString(),
            isShipped(tokenId) ? '-shipped.json' : '.json'
        ));
    }

    function claimFromFT(address to) public returns (uint) {
        // Throws if `_to` is zero address
        require(to != address(0), "ERC721: claim to the zero address");
        uint tokenId = this.totalSupply();
        require(tokenId < maxSupply, "Maximum supply already claimed");

        // Perform burn of one full FT
        fungibleContract.burnFrom(_msgSender(), 10**18);

        _safeMint(_msgSender(), tokenId);
        console.log("Claimed new token %s to %s", tokenId, to);
        return tokenId;
    }

    function isShipped(uint tokenId) public view virtual returns (bool) {
        return shippedStatus[tokenId];
    }

    function convertToShipped(uint[] memory tokenIds) public {
        for (uint i=0; i<tokenIds.length; i++) {
            uint tokenId = tokenIds[i];
            require(!isShipped(tokenId), "Already shipped");
            require(_isApprovedOrOwner(_msgSender(), tokenId), "Not owner of token");
            shippedStatus[tokenId] = true;
            console.log("Converted to shipped: %s", tokenId);
            emit Shipped(_msgSender(), tokenId);
        }
    }

    // OWNER FUNCTIONS //
    function setTokenBaseURI(string memory _tokenBaseUri) public onlyOwner {
        tokenBaseUri = _tokenBaseUri;
    }

    // Internal functions //
    function _baseURI() internal view override returns (string memory) {
        return tokenBaseUri;
    }

    function _beforeTokenTransfer(address from, address to, uint tokenId)
        internal
        override(ERC721, ERC721Enumerable)
    {
        super._beforeTokenTransfer(from, to, tokenId);
    }

    // Misc //
    function supportsInterface(bytes4 interfaceId)
        public
        view
        override(ERC721, ERC721Enumerable)
        returns (bool)
    {
        return super.supportsInterface(interfaceId);
    }
}
