//@ts-ignore
import pinataSDK from '@pinata/sdk';
//@ts-ignore
import { load } from 'config-yml';
import fs from 'node:fs/promises';

const IMAGE_EXTENSION = '.jpg'

const cfg = load()

export const generateMetaAndUpload = async function () {
  process.chdir('metadata/')
  
  // if (await fs.access('generated/'))
  await fs.rmdir('generated/', { recursive: true })
  await fs.mkdir('generated/')

  // PINATA INIT //
  if (!cfg.pinata.key) throw new Error("Missing config: pinata.key")
  if (!cfg.pinata.secret) throw new Error("Missing config: pinata.secret")
  const pinata = pinataSDK(cfg.pinata.key, cfg.pinata.secret)
  console.log("Pinata auth", await pinata.testAuthentication())

  // Check image count //
  let artworkCount=1;
  while (true) {
    try {
      await fs.access(`images/${artworkCount}${IMAGE_EXTENSION}`)
    } catch(err) {
      // console.error(err)
      if ((err as any).code !== 'ENOENT') throw err
      if (artworkCount == 1) throw new Error(`No image found: 'images/${artworkCount}${IMAGE_EXTENSION}'`)
      artworkCount--
      break
    }
    artworkCount++
  }

  // UPLOAD IMAGES //
  console.log("Uploading", artworkCount, "images")
  const imagePinResult = await pinata.pinFromFS('images', {
    pinataMetadata: {
      name: "WineSwap images"
    }
  })
  const imageFolderHash = imagePinResult.IpfsHash
  console.log("Images hash:", imageFolderHash)

  // GENRATE META //
  console.log("Generating JSON files")
  for (let index = 0; index < artworkCount; index++) {
    const template = (await fs.readFile('metadata.json')).toString();
    const artworkNumber = index + 1 // NFT ID starts at 0, artwork at 1

    // make one for unshipped and one for shipped
    await Promise.all([false, true].map(async shipFlag => {
      await fs.writeFile(
        `generated/${index}${shipFlag ? '-shipped' : ''}.json`,
        template
          .replace("<IMAGE_URL>", `https://cloudflare-ipfs.com/ipfs/${imageFolderHash}/${artworkNumber}${IMAGE_EXTENSION}`)
          .replace("<ARTWORK_NUMBER>", (artworkNumber).toString())
          .replace("<SHIP_STATUS>", shipFlag ? 'Shipped' : 'Unshipped')
      );
    }))
  }

  // UPLOAD META //
  const metaUploadResult = await pinata.pinFromFS('generated', {
    pinataMetadata: {
      name: "WineSwap metadata"
    }
  })
  console.log("Pin result:", metaUploadResult)
  const result = {
    artworkCount,
    imageHash: imageFolderHash,
    metaHash: metaUploadResult.IpfsHash,
    timestamp: metaUploadResult.Timestamp,
  };
  await fs.writeFile(`last-upload.json`, JSON.stringify(result, undefined, 4));
  return result
}

if (require.main === module) {
  generateMetaAndUpload().catch(console.error)
}
