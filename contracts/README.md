## IPFS hashes

Image: [`TODO`](https://cloudflare-ipfs.com/ipfs/TODO)

JSON: [`TODO`](https://cloudflare-ipfs.com/ipfs/TODO)

## Addresses

FT: [`TODO`](https://rinkeby.etherscan.io/address/TODO)

NFT: [`TODO`](https://rinkeby.etherscan.io/address/TODO)

## Setup

1. `yarn`
2. `cp config.sample.yml config.yml` (& add missing info)
3. `yarn test`
4. `yarn deploy --network rinkeby`
