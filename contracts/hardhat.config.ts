import '@nomiclabs/hardhat-ethers';
import "@nomiclabs/hardhat-vyper"; // for contract language "vyper"
import "@nomiclabs/hardhat-waffle"; // for tests
import chai from 'chai';
import chaiAsPromised from 'chai-as-promised';
import chaiSubset from 'chai-subset';
//@ts-ignore
import { load } from 'config-yml';
import 'hardhat-deploy';
import "@typechain/hardhat"; // generates typings from contracts
import { HttpNetworkUserConfig, NetworksConfig, NetworksUserConfig } from 'hardhat/types';
import { HardhatUserConfig, NetworkConfig } from "hardhat/types/config";
import _ from 'lodash';

chai.use(chaiSubset)
chai.use(chaiAsPromised)

const cfg = load()

const config: HardhatUserConfig = {
  solidity: "0.8.4",
  namedAccounts: {
    deployer: {
      default: 0,
      ..._.mapValues(cfg.networks, (net: any, name: string) => ({
        [name]: net.deployer.address as string,
      }))
    },
  },
  networks: {
    hardhat: {
      // saveDeployments: false, // https://github.com/nomiclabs/hardhat/issues/1365
    },
    ..._.mapValues(cfg.networks, (net: any, name: string) => ({
      url: net.rpc as string,
      chainId: net.chainId as number|undefined,
      accounts: [net.deployer.secret],
      // tags: ["staging"],
      gasPrice: net.gasPrice ?? 10000000000,
    } as NetworkConfig)),
  }
};
export default config;
