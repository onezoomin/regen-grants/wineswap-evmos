import { AnchorHTMLAttributes, FunctionComponent } from 'react'
import { explorerUrlForTx } from '../utils/main-utils'
import { LinkOut } from './minis'

export const EtherscanLink: FunctionComponent<AnchorHTMLAttributes<HTMLAnchorElement> &{
  trx: string
  networkName: string|null
}> = ({ trx, networkName, className, children, ...restProps }) => (
  <LinkOut
    href={networkName ? explorerUrlForTx(networkName, trx) : ''}
    title="View transaction on Etherscan"
    {...{ className, ...restProps }}
  >
    {children ?? (<>
      View transaction on Etherscan
    </>)}
  </LinkOut>
)
