import { ethers } from 'ethers'

import { useEthContext } from '../context/eth-data'
import { amountFormatter, FT_NAME, INITIAL_SUPPLY, NFT_NAME } from '../utils/main-utils'

const Content = ({ children }) => (
  <div className="flex flex-col mb-2 justify-between px-16  text-gray-400">
    {children}
  </div>
)

const Description = ({ children }) => (
  <div className="flex justify-between">
    {children}
  </div>
)

export default function Stats () {
  const { ethData } = useEthContext()
  const { totalSupplyWINE, /* totalSupplyUNIWINE,  */reserveWineTokens } = ethData
  return !totalSupplyWINE || !reserveWineTokens
    ? null
    : (
      <Content>

        <Description>
          <p>
            Initial {FT_NAME} FT
          </p>
          <p>{INITIAL_SUPPLY}</p>
        </Description>

        <Description>
          <p>
            Unsold {FT_NAME} FT
          </p>
          <p>{amountFormatter(reserveWineTokens, 18, 0)}</p>
        </Description>

        <Description>
          <p>
            Unclaimed {NFT_NAME} NFT
          </p>
          <p>{ INITIAL_SUPPLY - totalSupplyWINE.div(ethers.constants.WeiPerEther)}</p>
        </Description>

      </Content>

      )
}
