import { BigNumber, ethers } from 'ethers'
import find from 'lodash/find'
import get from 'lodash/get'
import last from 'lodash/last'
import { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { useAppContext } from '../context'
import { useEthContext } from '../context/eth-data'
import { useUniwineMeta } from '../hooks/main'
import question from '../img/question.svg'
import { useUIContext } from '../pages/Body/UIContext'
import { FT_NAME, MODAL_TYPES, NFT_NAME, sleep } from '../utils/main-utils'
import { rIF } from '../utils/react-utils'
import { isWalletCancelError } from '../utils/web3-utils'
import { ButtonsRow } from './ButtonsRow'
import { ButtonTW } from './ButtonTW'
import { ModalBack } from './ModalBack'
import ModalContent from './ModalContent'
import { NFTSingle } from './NFTPicker'
import { ProcessTracker } from './ProcessTracker'

export default function Claim () {
  let { ethData: { claimFromFT, balanceWINE, tokensUNIWINE, nextUNIWINE } } = useEthContext()
  // const { library } = useActiveWeb3React()
  const { appState, updateAppState } = useAppContext()
  const { process } = useEthContext()
  const { errorDialog } = useUIContext()

  const [amountSelected, setAmountSelected] = useState()
  const [claimedTokenID, setClaimedTokenID] = useState(null)
  const nextTokenMeta = get(useUniwineMeta(nextUNIWINE && [nextUNIWINE]), nextUNIWINE?.id)
  const claimedToken = claimedTokenID && find(tokensUNIWINE, { id: claimedTokenID })
  const claimedTokenMeta = get(useUniwineMeta(claimedToken && [claimedToken]), claimedTokenID)

  if (appState.fake) {
    claimFromFT = async function (count, process) {
      await process.trackStep({ type: 'approve', status: 'wait', hash: '0xaebb59ed8887010ebc6e35e8d7fe1049391f2ae2dad8ce6bd899385049773e31' },
        () => sleep(1000))
      await process.trackStep({ type: 'claim', status: 'wait', hash: '0x50d8a98ec306d01af5233eca7fcba797ed514cf26dee7e9c03b98f83ab34be7d' },
        () => sleep(1000))
      process.setSuccessful()
      return { events: [{ event: 'Transfer', args: { tokenId: BigNumber.from(last(tokensUNIWINE)?.id ?? 1) } }] } // can't use next as it wont be in the tokensUNIWINE list
    }
  }

  // Use effect to start process, as we need to wait for react state reset
  useEffect(() => {
    if (process.active && !process.steps.length) {
      setAmountSelected(appState.count)
      claimFromFT(appState.count, process)
        .then(receipt => {
          const events = receipt.events.filter(ev => ev.event === 'Transfer')
          if (!events.length) console.error('Receipt has no Transfer event:', receipt)
          if (events.length > 1) console.warn('Receipt has multiple Transfer events:', events)
          setClaimedTokenID(events[0].args.tokenId.toNumber())
        })
        .catch(error => {
          if (!isWalletCancelError(error)) {
            return errorDialog('Failed to claim', error)
          }
        })
    }
  }, [claimFromFT, process, appState.count, appState.fake, errorDialog])

  if (!amountSelected || !process.success) {
    const hasEnough = balanceWINE && balanceWINE.div(ethers.constants.WeiPerEther).gte(appState.count)

    return (
      <ModalContent
        img={<NFTSingle className="w-full h-full inline-block -mx-0" token={nextUNIWINE} meta={nextTokenMeta} />}
        renderInfo={() => <p className="text-center">This is the next available NFT</p>}
      >
        <ProcessTracker process={process} />

        {process.steps.length
          ? null
          : (
            <div className="mt-1 text-sm text-center">
              <p className="">Swap 1 {FT_NAME} FT for 1 {NFT_NAME} NFT with this artwork<br/>
                (there might be race conditions).<br/>
                <br />
                You can trade the NFT or redeem it for a shipped bottle.
              </p><br/>
            </div>
            )}

        <div className="w-full flex items-center justify-center m-0 mt-4">
          {/* {!process.active
            ? <IncrementToken
              className="mr-4 my-4"
              initialValue={1 /* Number(amountFormatter(balanceWINE, 18, 0)) * /}
              max={1/* Number(amountFormatter(balanceWINE, 18, 0)) * /}
            />
            : null} */}
          <ButtonTW
            className={`flex-initial ${process.active ? ' w-full' : ''}`}
            pending={process.active}
            disabled={process.active || !hasEnough}
            onClick={() => process.start()}
          >
            {hasEnough ? process.active ? 'Claiming' : 'Claim' : `No ${FT_NAME}`}
          </ButtonTW>
          {rIF(!process.active, <Link className="inline-block ml-4 p-2" to="/faq#Claim">
            <img className="h-4" src={question} alt="help / info button" />
          </Link>)}
        </div>
      </ModalContent>
    )
  } else {
    return (
      <ModalContent
        img={<NFTSingle className="w-full h-full inline-block -mx-0" token={claimedToken} meta={claimedTokenMeta} />}
      >
        <div className="mb-4">
          <p className="text-md text-center">You successfully claimed {amountSelected} {NFT_NAME} NFT!</p>
        </div>

        <ProcessTracker process={process} />

        {/* <ButtonTW
          buttonType="secondary"
          className="max-w-fit mb-2"
          onClick={() => requestWalletTokenAdd(library, UNIWINE)}
        >
          Add to Wallet
        </ButtonTW> */}

        <ButtonsRow>
          <ModalBack className="pl-0" onClick={() => {
            process.reset()
            setAmountSelected(null)
          }}
          />
          <ButtonTW
            className="max-w-fit"
            onClick={() => {
              process.reset()
              updateAppState({ tradeType: MODAL_TYPES.REDEEM })
            }}
          >
            Ship
          </ButtonTW>
        </ButtonsRow>
      </ModalContent>
    )
  }
}
