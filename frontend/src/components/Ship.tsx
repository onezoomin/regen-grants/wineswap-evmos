import { Signer } from '@ethersproject/abstract-signer'
import { Web3Provider } from '@ethersproject/providers'
import { ethers } from 'ethers'
import filter from 'lodash/filter'
import union from 'lodash/union'
import { Dispatch, SetStateAction, useEffect, useMemo, useState } from 'react'
import { Link } from 'react-router-dom'
import { useAppContext } from '../context'
import { useEthContext } from '../context/eth-data'
import { EthData } from '../hooks/eth-data'
import { useUniwineMeta } from '../hooks/main'
import { Process } from '../hooks/process'
import { useActiveWeb3React } from '../hooks/web3'
import question from '../img/question.svg'
import { n8nOrder } from '../network/n8n'
import { UIContextType, useUIContext } from '../pages/Body/UIContext'
import { ackeeShipShip } from '../utils/ackee'
import { NFT_NAME, sleep } from '../utils/main-utils'
import { rIF } from '../utils/react-utils'
import { ShipButtons } from './Buttons'
import { ButtonTW } from './ButtonTW'
import { LoaderOverlay } from './Loader'
import { ModalBack } from './ModalBack'
import ModalContent from './ModalContent'
import { NFTPicker, NFTSingle } from './NFTPicker'
import { ProcessTracker } from './ProcessTracker'
import RedeemForm from './ShipForm'

async function performRedeemProcess (
  { flipToShipped, process, library, account, userAddress, whichSelected, setSignedMessage, setSignature, fake, errorDialog }:
  { flipToShipped: EthData['flipToShipped'], process: Process, library: Web3Provider, account: string, userAddress: string, whichSelected: number[], setSignedMessage: Dispatch<SetStateAction<string>>, setSignature: Dispatch<SetStateAction<string>>, fake: boolean, errorDialog: UIContextType['errorDialog']},
) {
  try {
    // Request signature //
    let message = ''; let signature = ''
    await process.trackStep({ type: 'sign', status: 'prompt' }, async updateStep => {
      const timestampToSign = new Date().toISOString()
      const header = 'PLEASE VERIFY YOUR ADDRESS (physical and wallet)!\nYour data is only sent to us and will never be shared publicly.'
      const formDataMessage = userAddress
      const autoMessage = `ETH Wallet: ${account}\nTimestamp: ${timestampToSign}\nNFT IDs: ${whichSelected.join(', ')}`

      // Request Signature
      const signer = library.getSigner() as Signer
      message = `${header}\n\n${formDataMessage}\n${autoMessage}`
      console.log('signedMessage', message)
      signature = await signer.signMessage(message)
      console.log('signature', signature)
      updateStep({ status: 'wait' })

      // Check Signature
      const verifyResult = ethers.utils.verifyMessage(message, signature)
      console.log('Verification result:', verifyResult)
      if (verifyResult !== account) throw new Error(`Verification of signed message failed: ${verifyResult}`)

      // Save  Signature
      setSignedMessage(message)
      setSignature(signature)
    })

    // Perform flip on chain //
    if (fake) {
      await process.trackStep({ type: 'flip', status: 'wait', hash: '0x849112f18e2341cb79aca31153203bc362454d1378e67724adbe278817a9f9ba' },
        async () => { await sleep(1000) })
    } else {
      await flipToShipped(whichSelected, process, { dontFinishProcess: true })
      // Submit to n8n //
      const email = userAddress.split('\n').pop()
      const orderJson = {
        signedMessage: message,
        shippingAddress: userAddress,
        signature,
        account,
        nftList: whichSelected,
        email: email ?? 'unknown',
      }
      await n8nOrder(orderJson, process)
    }

    // const email = userAddress.split('\n').pop()
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    // const { placedOrderId } = await moralisCreateOrder(account, message!, userAddress, signature!, whichSelected, email ?? 'unknown', process)
    // setMoralisPlacedOrderId(placedOrderId)

    await sleep(500)
    process.setSuccessful()
  } catch (error) {
    errorDialog('Redeem process failed', error)
  } finally {
    process.setInactive()
  }
}

export default function Redeem ({ setConfetti }) {
  const {
    ethData: { flipToShipped, balanceUNIWINE, tokensUNIWINE, UNIWINE },
    process,
  } = useEthContext()
  const { library, account } = useActiveWeb3React()
  const { appState } = useAppContext()
  const { errorDialog } = useUIContext()

  const [whichSelected, setWhichSelected] = useState<number[]>([])
  const [confirmedSelection, setConfirmedSelection] = useState(false)
  const [hasConfirmedAddress, setHasConfirmedAddress] = useState(false)
  const [signedMessage, setSignedMessage] = useState<string|null>(null)
  const [signature, setSignature] = useState<string|null>(null)
  const [moralisPlacedOrderId, setMoralisPlacedOrderId] = useState<string|null>(null)

  const [userAddress, setUserAddress] = useState<string|null>(null)

  const erosUnredeemed = useMemo(() => tokensUNIWINE && filter(tokensUNIWINE, { shipped: false }), [tokensUNIWINE])
  const tokensSelected = useMemo(() => whichSelected && tokensUNIWINE && filter(tokensUNIWINE, t => whichSelected.includes(t.id)), [tokensUNIWINE, whichSelected])
  const tokenMeta = useUniwineMeta(union(erosUnredeemed, tokensSelected))
  const redeemedCount = erosUnredeemed && balanceUNIWINE ? balanceUNIWINE.toNumber() - erosUnredeemed.length : null
  console.debug({ erosUnredeemed, tokensSelected, union: union(erosUnredeemed, tokensSelected), tokenMeta })

  // Show confetti when process is
  useEffect(() => {
    setConfetti(process.success)
  }, [process.success, setConfetti])

  // Use effect to start process, as we need to wait for react state reset
  useEffect(() => {
    if (process.active && !process.steps.length) {
      if (!library || !account || !userAddress) errorDialog('Failed to redeem', new Error('missing Web3 or invalid state'))
      else {
        performRedeemProcess({ flipToShipped, process, library, account, userAddress, whichSelected/* , setMoralisPlacedOrderId */, setSignedMessage, setSignature, fake: appState.fake, errorDialog })
      }
    }
  }, [account, flipToShipped, library, process, appState.fake, userAddress, whichSelected, errorDialog])

  function renderOrderInfo () {
    return <div className="text-center">
      <p style={{ fontSize: '16px' }}>{whichSelected.length} {NFT_NAME} NFT{whichSelected.length > 1 ? 's' : ''}</p>
      <p style={{ fontSize: '16px' }}>Shipped {rIF(hasConfirmedAddress, 'to: ')}</p>
      <p className="text-left whitespace-pre-wrap font-light py-2" style={{ fontSize: '14px' }}>{!hasConfirmedAddress ? 'from: Carvalhal, Portugal' : userAddress}</p>
    </div>
  }

  function renderNFTs () {
    if (whichSelected.length === 1) { return <NFTSingle className="w-full h-full inline-block -mx-0" token={tokensSelected[0]} meta={tokenMeta[whichSelected[0]]} /> } else {
      return <NFTPicker
        tokens={tokensSelected}
        tokenMeta={tokenMeta}
        tokenType={UNIWINE}
        selected={null}
      />
    }
  }

  if (!confirmedSelection) {
    return (
      <ModalContent
        id="RedeemModal"
        className="relative"
        contentClasses="pt-0"
        img={null}
        renderInfo={() => null}
      >
        {rIF(erosUnredeemed === null, <LoaderOverlay size="30px"></LoaderOverlay>)}

        <NFTPicker
          tokens={erosUnredeemed}
          tokenMeta={tokenMeta}
          tokenType={UNIWINE}
          selected={whichSelected}
          setSelected={setWhichSelected}
        />

        {!balanceUNIWINE
          ? null
          : <div className="mb-2 text-center text-sm text-gray-400">
            {`You own ${erosUnredeemed?.length ?? '?'} unshipped ${NFT_NAME} NFT ${(redeemedCount ? ` (+ ${redeemedCount} shipped) ` : '')}`}
          </div>}

        <div className="mb-6 antialiased text-sm text-center">
          <p className="">{`Redeem your ${NFT_NAME} NFT for a shipped bottle. You will keep the NFT, and it will be flagged as 'shipped'.`}</p>
          {/* <p className="text-red-300">This process is irreversible!</p> */}
        </div>

        <div className="mb-4  w-full flex items-center justify-center">
          <ButtonTW
            disabled={!whichSelected.length}
            onClick={() => {
              ackeeShipShip()
              setConfirmedSelection(true)
            }}
          >
            {!whichSelected.length ? 'Select Tokens' : `Select ${whichSelected.length}`}
          </ButtonTW>
          {rIF(!process.active, <Link className="inline-block ml-4 p-2" to="/faq#Ship">
            <img className="h-4" src={question} alt="help / info button" />
          </Link>)}
        </div>
      </ModalContent>
    )
  } else if (!hasConfirmedAddress) {
    return (
      <ModalContent
        id="RedeemModal"
        contentClasses="pt-0"
        img={renderNFTs()}
        renderInfo={renderOrderInfo}
      >
        <p className="text-gray-200 w-full">Where should we send them?</p>
        <RedeemForm
          setHasConfirmedAddress={setHasConfirmedAddress}
          setUserAddress={setUserAddress}
        />
        <ModalBack onClick={() => setConfirmedSelection(false)} />
      </ModalContent>
    )
  } else if (!process.success) {
    return (
      <ModalContent
        id="RedeemModal"
        contentClasses="pt-0"
        img={renderNFTs()}
        renderInfo={renderOrderInfo}
      >
        <ProcessTracker process={process} />

        <ButtonTW
          className="w-full mb-4"
          pending={process.active}
          disabled={process.active}
          onClick={() => process.start()}
        >
          {process.active ? 'Redeeming' : 'Place order'}
        </ButtonTW>

        {process.active
          ? null
          : <ModalBack onClick={() => {
            setHasConfirmedAddress(false)
            process.reset()
          }}
          />}
      </ModalContent>
    )
  } else {
    return (
      <ModalContent id="RedeemModal" contentClasses="pt-0"
        img={renderNFTs()} renderInfo={renderOrderInfo}
      >
        <ProcessTracker process={process} />

        <div className="mt-4 w-full text-center">
          <p>
            Estimated shipping time 2-3 weeks. <br /> Shipping time will vary by region.
          </p>
          <p>
            Your shipping status can be viewed <Link className="underline" to="/status">here</Link>.
          </p>
        </div>

        <p className="w-full my-4 text-center">Contact us on discord if you want<br />(otherwise we&apos;ll send emails):</p>
        <ShipButtons
          className="mb-4"
          {...{ signedMessage, signature, account, moralisPlacedOrderId }}
          onFinished={() => {}}
        />
      </ModalContent>
    )
  }
}
