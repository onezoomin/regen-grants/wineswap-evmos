import { Delete } from '@material-ui/icons'
import QRCode from 'qrcode.react'
import { useEffect, useState } from 'react'
import { useActiveWeb3React } from '../hooks/web3'
import { injected, portis, torus, walletconnect } from '../network/connectors'
import { rIF } from '../utils/react-utils'
import { ButtonTW } from './ButtonTW'

export const QRCodeWrapper = ({ className = '', children }) => {
  className = `w-full text-center mt-4 ${className}`
  return (
    <div {...{ className }}>
      {children}
    </div>
  )
}

export default function Connect ({ setShowConnect }) {
  const { account, connector, activate } = useActiveWeb3React()
  const walletconnectUri = connector && connector.walletConnector && connector.walletConnector.uri

  // connector error
  const [connectorError, setConnectorError] = useState()

  async function activateConnector (connector) {
    try {
      console.log('Activating connector:', connector)
      await activate(connector, undefined, true)
      console.log('Activated successfully')
    } catch (error) {
      console.warn('connector error:', error)
      setConnectorError(error)
    }
  }

  // unset the error on connector change
  useEffect(() => {
    setConnectorError()
  }, [connector])

  // once an account is connected, don't show this screen
  useEffect(() => {
    if (account !== null) {
      setShowConnect(false)
    }
  })

  const walletConnectHasConnection = walletconnect.walletConnectProvider?.accounts

  return (
    <div id="Connect" className="w-full h-full flex flex-row items-center p-6 pt-8"> {/* maxWidth: 20em set on surrounding Modal (Modal.jsx) */}
      <div id="ConnectInner" className="w-full h-max-content flex flex-col items-center text-center text-white">
        {rIF(connectorError, () => <p style={{ width: '100%', textAlign: 'center', marginBottom: '12px', color: 'red' }}>
          {'Connection Error: ' + connectorError.message}<br/>
          <a href="https://ethereum.org/use/#_3-what-is-a-wallet-and-which-one-should-i-use" style={{ textDecoration: 'underline', color: 'white' }}>How to set up a wallet?</a>
        </p>)}

        <div className="w-full rounded-md flex flex-col mb-6" style={{ maxWidth: '17em' }}>
          {/* Injected */}
          <ButtonTW onClickWithLoad={() => activateConnector(injected)} className="mb-2">
            Browser Wallet
          </ButtonTW>

          {/* WalletConnect */}
          <div className="flex items-stretch">
            <ButtonTW className="flex-1" onClickWithLoad={() => activateConnector(walletconnect)}>
              <img className="h-6 mb-1.5 mt-0.5" src="https://raw.githubusercontent.com/WalletConnect/walletconnect-assets/master/svg/walletconnect-banner.svg" alt="WalletConnect" />
            </ButtonTW>
            {rIF(walletConnectHasConnection,
              <ButtonTW
                title={walletConnectHasConnection?.join(', ')}
                className="ml-1 h-auto"
                onClickWithLoad={async () => {
                  await walletconnect?.walletConnectProvider?.disconnect()
                  window.location.reload()
                }}
              >
                <Delete />
              </ButtonTW>)}
          </div>
        </div>

        <div className="w-full rounded-md flex flex-col mb-6" style={{ maxWidth: '17em' }}>
          <span className="text-sm text-center mb-3">For the less tech&#8209;savy</span>

          {/* Torus */}
          <ButtonTW onClickWithLoad={() => activateConnector(torus)} className="mb-2">
            <img className="h-8" src="https://docs.tor.us/images/wallet-full-logo.svg" alt="Torus Wallet" />
          </ButtonTW>

          {/* Portis */}
          <ButtonTW onClickWithLoad={() => activateConnector(portis)} className="">
            <img className="h-6 m-1" src="https://docs.portis.io/_media/logo.svg" alt="Portis" />
          </ButtonTW>
        </div>

        {rIF(!connectorError, () => <p className="w-full text-sm">
          Don&apos;t have one?{' '}
          <a href="https://ethereum.org/use/#_3-what-is-a-wallet-and-which-one-should-i-use" style={{ textDecoration: 'underline' }}>Learn more</a>
        </p>)}

        {rIF(walletconnectUri && account === null && !connectorError,
          <QRCodeWrapper>
            <QRCode value={walletconnectUri} />
            <p>Scan to connect</p>
          </QRCodeWrapper>,
        )}
      </div>
    </div>
  )
}
