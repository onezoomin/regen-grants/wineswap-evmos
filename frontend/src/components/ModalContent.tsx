import { ReactNode, useContext } from 'react'
import { extendClasses, rIF } from '../utils/react-utils'
import { ModalContext } from './Modal'

export default function ModalContent ({
  id = 'ModalContent',
  className = '',
  contentClasses = '',
  img = null,
  renderInfo = null,
  children,
}: {
  id: string
  className?: string
  contentClasses?: string
  img: string|ReactNode|null
  renderInfo: (() => ReactNode)|null
  children: ReactNode
}) {
  const { contentRef: ref } = useContext(ModalContext)

  return (
    <div {...{ id, ref }} className={extendClasses(className, 'w-full max-h-full flex flex-col justify-start items-center text-gray-50 overflow-x-hidden overflow-y-auto')}>
      <div id="ModalContent-TopFrame" className="w-full user-select-none">
        {rIF(img, typeof img !== 'string'
          ? img
          : <img className="m-0" src={img} alt="Logo" draggable="false" />)}
        {rIF(renderInfo, <div id="ModalContent-InfoFrame" className={'w-full flex flex-row flex-nowrap justify-around items-center mt-4 px-4 text-sm'}>
          {renderInfo?.()}
        </div>)}
      </div>
      <div className={extendClasses(contentClasses, 'p-4 w-full flex flex-col justify-start items-center')}>{children}</div>
    </div>
  )
}
