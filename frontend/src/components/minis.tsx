import { SvgIcon } from '@material-ui/core'
import { FiberManualRecord } from '@material-ui/icons'
import { AnchorHTMLAttributes, FunctionComponent } from 'react'
import { ReactComponent as SignalSVG } from '../img/SignalLogo.svg'
import { ReactComponent as N8nSVG } from '../img/n8n.svg'
import { appendClassNames } from '../utils/react-utils'

export const TitleH2 = ({ children = 'Place Holder', m = '2', className = '' }) => (
  <h2 className={`mx-${m} mb-${m} text-2xl text-center tracking-wider ${className}`}>{children}</h2>
)

export const LinkOut: FunctionComponent<AnchorHTMLAttributes<HTMLAnchorElement>> = ({ className, children, ...restProps }) => {
  return <a
    {...appendClassNames('underline', className)}
    {...restProps}
    target="_blank"
    rel="noopener noreferrer"
  >
    {children}
  </a>
}
export const BouncingEllipsis = ({ className }) => {
  className = `flex flex-row max-w-fit justify-between animate-pulse ${className}`
  return (
    <div
      {...{ className }}
    >
      <FiberManualRecord className="max-h-2 max-w-3 animate-bounce"/>
      <FiberManualRecord className="max-h-2 max-w-3 animate-bounce animation-delay-100"/>
      <FiberManualRecord className="max-h-2 max-w-3 animate-bounce-d200"/>
    </div>
  )
}
export const FlexRow = ({ className = '', children }) => {
  className = `flex flex-row ${className}`
  return (
    <div {...{ className }}>
      {children}
    </div>
  )
}
export const SignalIcon = ({ className = '', ...props }) => {
  className = `h-5 ${className}`
  return (
    <SvgIcon {...props} {...{ className }} component={SignalSVG} viewBox="0 0 32 32"/>
    // <img {...props} {...{ className }} src={SignalSVG} viewBox="0 0 32 32"/>
  )
}
export const N8nIcon = ({ className = '', ...props }) => {
  className = `h-5 text-gray-50 ${className}`
  return (
    <SvgIcon {...props} {...{ className }} component={N8nSVG} viewBox="0 0 180 180"/>
    // <img {...props} {...{ className }} src={SignalSVG} viewBox="0 0 32 32"/>
  )
}
