import { BigNumber } from '@ethersproject/bignumber'
import { ETHER, Token } from '@uniswap/sdk-core'
import { Route, Router, Trade } from '@uniswap/v2-sdk'
import { ethers } from 'ethers'
import filter from 'lodash/filter'
import fromPairs from 'lodash/fromPairs'
import { useCallback, useMemo, useState } from 'react'
import { useAppContext } from '../context'
import { useActiveWeb3React } from '../hooks/web3'
import { ackeeBuyFunnel, ackeeBuySuccess } from '../utils/ackee'
import { nullish } from '../utils/constants'
import { ERROR_CODES, fractionToBigNumber, FT_NAME, MODAL_TYPES, parseTradeAmount, UNIWINE_ABI } from '../utils/main-utils'
import { NFT_NAME } from './../utils/main-utils'
import { ALLOWED_SLIPPAGE_PERCENT, calculateGasMargin, calculateSlippageBounds, calculateTrade, estimateGasPrice, requestApproval } from './eth-data-functions'
import {
  useAddressAllowance,
  useAddressBalance,
  useContract,
  useDetectNetwork, useExchangePair, useNextUniwineToken, useTokenContract,
  useTotalSupply, useUniwineTokenList,
} from './main'
import { Process } from './process'

// denominated in seconds
export const DEADLINE_FROM_NOW = 60 * 15

let changeTracker: any = {}

export function useEthDataCallOnlyFromContextInit () { // named like that so that one does not accidentially use this hook (use useEthContext())
  const web3 = useActiveWeb3React()
  const { appState: { fullAllowance } } = useAppContext()
  const { account, chainId, library } = web3

  // network
  const { addresses, routerContract, networkError } = useDetectNetwork()

  // Tokens
  const [selectedTokenSymbol, setSelectedTokenSymbol] = useState('ETH')
  const ETH = ETHER
  const WETH = useMemo(() => chainId && addresses && new Token(chainId, addresses.WETH, 18, 'WETH'), [chainId, addresses])
  const WINE = useMemo(() => chainId && addresses && new Token(chainId, addresses.WINE, 18, 'WINE'), [chainId, addresses])
  const UNIWINE = useMemo(() => chainId && addresses && new Token(chainId, addresses.UNIWINE, 0, 'UNIWINE'), [chainId, addresses])
  const DAI = useMemo(() => chainId && addresses && new Token(chainId, addresses.DAI, 18, 'DAI'), [chainId, addresses])
  const SELECTED = useMemo(
    () => (chainId && addresses && selectedTokenSymbol !== 'ETH')
      ? new Token(chainId, addresses[selectedTokenSymbol], 18, selectedTokenSymbol)
      : null,
    [chainId, addresses, selectedTokenSymbol],
  )

  // get exchange contracts
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [pairEthWine, contractEthWine] = useExchangePair(WETH, WINE)
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [pairEthSelected, contractEthSelected] = useExchangePair(WETH, SELECTED)
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [pairEthDai, contractEthDai] = useExchangePair(WETH, DAI)

  // get token contracts
  const tokenContractWINE = useTokenContract(WINE?.address)
  const tokenContractUNIWINE = useContract(UNIWINE?.address, UNIWINE_ABI)
  const tokenContractSelected = useTokenContract(SELECTED?.address)

  // get balances
  const balanceETH = useAddressBalance(account, 'ETH')
  // const balanceWETH = useAddressBalance(account, WETH?.address) // TODO: Automatically use WETH when available
  const balanceWINE = useAddressBalance(account, WINE?.address)
  const balanceUNIWINE = useAddressBalance(account, UNIWINE?.address)
  const balanceSelected = useAddressBalance(account, SELECTED?.address)

  // totalsupply
  const totalSupplyWINE = useTotalSupply(tokenContractWINE)
  const totalSupplyUNIWINE = useTotalSupply(tokenContractUNIWINE)

  // UNIWINE tokens
  const tokensUNIWINE = useUniwineTokenList(account, UNIWINE?.address)
  const nextUNIWINE = useNextUniwineToken(library, UNIWINE?.address, totalSupplyUNIWINE)

  // get allowances
  const allowanceSwapWINE = useAddressAllowance(account, WINE, routerContract?.address)
  const allowanceSwapSelected = useAddressAllowance(account, SELECTED, routerContract?.address)
  const allowanceClaimWINE = useAddressAllowance(account, WINE, UNIWINE)
  // console.log({ allowanceSwapWINE, allowanceSwapSelected, allowanceClaimWINE })

  // get reserves
  // const reserveWINEETH = useAddressBalance(exchangeContractWINE && exchangeContractWINE.address, ETH)
  // const reserveWineTokens = useAddressBalance(
  //   exchangeContractWINE && exchangeContractWINE.address,
  //   WINE
  // )
  // const { reserveETH: reserveSelectedTokenETH, reserveToken: reserveSelectedTokenToken } = useExchangeReserves(
  //   selectedTokenAddress
  // )
  const reserveWineTokens = WINE && pairEthWine?.reserveOf(WINE)
  // console.log(!!addresses, {
  //   token1Price: exchangeEthWine?.token1Price.toSignificant(), token0Price: exchangeEthWine?.token0Price.toSignificant(),
  //   reserveWineTokens: reserveWineTokens,
  //   reserve0: exchangeEthWine?.reserve0.toSignificant(),
  //   exchangePairWINE: exchangeEthWine
  // })

  // const reserveDAIETH = useAddressBalance(exchangeContractDAI && exchangeContractDAI.address, ETH)
  // const reserveDAIToken = useAddressBalance(exchangeContractDAI && exchangeContractDAI.address, DAI)

  const rateEthUsd = useMemo(() => WETH && pairEthDai?.priceOf(WETH), [pairEthDai, WETH])
  const rateWineEth = useMemo(() => WINE && pairEthWine?.priceOf(WINE), [pairEthWine, WINE])
  const rateWineUsd = useMemo(
    () => rateEthUsd != null ? rateWineEth?.multiply(rateEthUsd) : null,
    [rateEthUsd, rateWineEth],
  )
  // const rateWineUsdR = useMemo(() => (exchangeEthWine != null) && (exchangeEthDai != null) ? new Route([exchangeEthWine, exchangeEthDai], WINE).midPrice : null, [exchangeEthWine, exchangeEthDai, WINE])
  // console.log({
  //   rateEthUsd: rateEthUsd?.toSignificant(3),
  //   rateWineEth: rateWineEth?.toSignificant(3),
  //   rateWineUsd: rateWineUsd?.toSignificant(3),
  //   rateWineUsdR: rateWineUsdR?.toSignificant(3),
  // })

  const ready = !!(
    // (account === null || allowanceWINE) &&
    // (selectedTokenSymbol === 'ETH' || account === null || allowanceSelectedToken) &&
    (account === null || balanceETH != null)
    && (account === null || balanceWINE != null)
    && (account === null || selectedTokenSymbol === 'ETH' || balanceSelected != null)
    // reserveWINEETH &&
    && (reserveWineTokens != null)
    // (selectedTokenSymbol === 'ETH' || reserveSelectedTokenETH) &&
    // (selectedTokenSymbol === 'ETH' || reserveSelectedTokenToken) &&
    && (pairEthWine != null)
    && (routerContract != null))

  // if (!ready) {
  //   console.info('Not ready:', {
  //     balETH: (account === null || (balanceETH != null)),
  //     balWINE: (account === null || balanceWINE != null),
  //     balSel: (account === null || selectedTokenSymbol === 'ETH' || balanceSelectedToken != null),
  //     resWine: (reserveWineTokens != null),
  //     exWine: (pairEthWine != null),
  //     router: (routerContract != null),
  //   })
  // }

  // console.log({
  //   ready,
  //   account,
  //   block,
  //   balanceETH: balanceETH?.toString(),
  //   balanceWETH: balanceWETH?.toString(),
  //   balanceWINE: balanceWINE?.toString(),
  //   balanceSelectedToken: balanceSelectedToken?.toString(),
  // })

  const validateTrade = useCallback(function (tradeType: string, route: Route, amount: BigNumber, tokenForAmount: Token) {
    console.debug('validateTrade', route, amount, tokenForAmount)
    let trade: Trade
    try {
      trade = calculateTrade(
        route,
        amount,
        tokenForAmount,
      )
    } catch (error) {
      console.error('requiredAmount calc error', error)
      error.code = ERROR_CODES.INVALID_TRADE
      throw error
    }

    const inputAmount = fractionToBigNumber(trade.inputAmount)
    const outputAmount = fractionToBigNumber(trade.outputAmount)
    const slippageBounds = calculateSlippageBounds(trade)

    // the following are 'non-breaking' errors that will still return the data
    let errorAccumulator

    // validate minimum selected token balance
    let balance
    if (route.input === ETH) balance = balanceETH
    if (route.input === SELECTED) balance = balanceSelected
    if (route.input === WINE) balance = balanceWINE
    if (!balance?.gte(slippageBounds.maximumAmountIn)) {
      const error: Error & {code?: String} = Error()
      error.code = ERROR_CODES.INSUFFICIENT_SELECTED_TOKEN_BALANCE
      if (!errorAccumulator) {
        errorAccumulator = error
      }
    }

    // validate minimum ether balance
    if (balanceETH?.lt(ethers.utils.parseEther('.01'))) { // TODO: is that really the gas amount required?
      const error: Error & {code?: String} = Error()
      error.code = ERROR_CODES.INSUFFICIENT_ETH_GAS
      if (!errorAccumulator) {
        errorAccumulator = error
      }
    }

    // validate allowance
    let allowance: BigNumber|nullish
    let allowanceNeeded = false
    if (route.input === ETH) allowance = null
    else if (route.input === SELECTED) allowance = allowanceSwapSelected
    else if (route.input === WINE) allowance = allowanceSwapWINE
    else throw new Error(`Unhandled input: ${route.input.symbol}`)
    if (allowance) {
      if (slippageBounds.maximumAmountIn && allowance.lt(slippageBounds.maximumAmountIn)) {
        allowanceNeeded = true
      }
    }

    return {
      trade,
      inputAmount,
      outputAmount,
      slippageBounds,
      allowanceNeeded,
      error: errorAccumulator,
    }
  }, [ETH, SELECTED, WINE, allowanceSwapSelected, allowanceSwapWINE, balanceETH, balanceSelected, balanceWINE])

  // buy functionality
  const validateBuy = useCallback(
    numberOfWINE => {
      if (
        WINE == null
        || (SELECTED != null && pairEthSelected == null)
        || pairEthWine == null
      ) return {} // not ready

      // validate passed amount
      const parsedValue = parseTradeAmount(numberOfWINE)

      return validateTrade(
        MODAL_TYPES.BUY,
        new Route(
          (pairEthSelected != null) ? [pairEthSelected, pairEthWine] : [pairEthWine],
          SELECTED ?? ETH,
        ),
        parsedValue,
        WINE,
      )
    },
    [ETH, SELECTED, WINE, pairEthSelected, pairEthWine, validateTrade],
  )
  const performTrade = useCallback(async (trade: Trade, slippageBounds, process: Process) => {
    try {
      if (!library || !account || !routerContract || !WINE || !allowanceSwapWINE || !tokenContractWINE) {
        throw new Error(`Web3 not ready ${!!library} ${!!account} ${!!routerContract}`)
      }
      if (SELECTED
        ? (!allowanceSwapSelected || !tokenContractSelected) // if we have a selected token, check if the contracts are ready
        : ![WINE.symbol, 'ETH'].includes(trade.route.input.symbol) // if not, check that the trade isn't trying to trade with SELECTED
      ) {
        throw new Error(`Selected token not ready ${trade.route.input.symbol} ${!!SELECTED} ${!!allowanceSwapSelected} ${!!tokenContractSelected}`)
      }
      console.trace('performTrade called', trade, slippageBounds)

      // Approve Uniswap contract to use token (if necessary)
      if (trade.route.input.symbol === 'ETH') {
        console.debug('[performTrade] input=ETH - no allowance needed')
      } else {
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        const token = trade.route.input.symbol === WINE.symbol ? WINE : SELECTED! // we check if SELECTED token is ready at the top of this function
        const allowance = token === WINE ? allowanceSwapWINE : allowanceSwapSelected
        if (!allowance) throw new Error(`Failed to get allowance for ${token.symbol}`)

        if (allowance.lt(slippageBounds.maximumAmountIn)) {
          await process.trackStep({ type: 'approve', status: 'prompt', info: `Approve Uniswap router to use your ${token.symbol}` }, async updateStep => {
            const tokenContract = token === WINE ? tokenContractWINE : tokenContractSelected
            if (!tokenContract) throw new Error(`Failed to get contract for ${token.symbol}`)

            const response = await requestApproval(library, tokenContract, routerContract.address, fullAllowance ? ethers.constants.MaxUint256 : slippageBounds.maximumAmountIn)
            updateStep({ hash: response.hash, status: 'wait', hasCompletionListener: true })
            const receipt = await response.wait()
            console.debug('[performTrade] approve receipt', receipt)
          })
        }
      }

      let receipt
      await process.trackStep({ type: 'trade', status: 'prompt' }, async updateStep => {
        const { methodName, args, value } = Router.swapCallParameters(trade, {
          ttl: DEADLINE_FROM_NOW,
          allowedSlippage: ALLOWED_SLIPPAGE_PERCENT,
          recipient: account,
        })
        console.log(`Calling Router.${methodName}(`, args, `) with ${value} Wei`)
        const response = await routerContract[methodName](...args, { value })
        updateStep({ hash: response.hash, status: 'wait', hasCompletionListener: true })
        receipt = await response.wait()
        console.debug('[performTrade] receipt', receipt)
      })
      ackeeBuyFunnel('success')
      ackeeBuySuccess()
      process.setSuccessful()
      return receipt
    } finally {
      process.setInactive()
    }
  }, [SELECTED, WINE, account, allowanceSwapSelected, allowanceSwapWINE, fullAllowance, library, routerContract, tokenContractSelected, tokenContractWINE])

  // sell functionality
  const validateSell = useCallback(
    numberOfWINE => {
      console.debug('validateSell', numberOfWINE)
      if (
        !WINE || !pairEthWine
        || (SELECTED != null && pairEthSelected == null)
      ) return {} // not ready

      // validate passed amount
      const parsedValue = parseTradeAmount(numberOfWINE)

      const tradeResult = validateTrade(
        MODAL_TYPES.SELL,
        new Route(
          (pairEthSelected != null) ? [pairEthWine, pairEthSelected] : [pairEthWine],
          WINE,
        ),
        parsedValue,
        WINE,
      )
      console.debug('validateSell result', tradeResult)
      return tradeResult
    },
    [SELECTED, WINE, pairEthSelected, pairEthWine, validateTrade],
  )

  const claimFromFT = useCallback(async (amount: number, process: Process) => {
    try {
      if (!library || !tokenContractUNIWINE || !tokenContractWINE) throw new Error('web3 is not ready')
      if (amount > 1) throw new Error('TODO: amount>1')
      const amountWei = ethers.constants.WeiPerEther.mul(amount)

      // Approve Uniwine contract to burn
      if (!allowanceClaimWINE || allowanceClaimWINE.div(ethers.constants.WeiPerEther).lt(amount)) {
        await process.trackStep({ type: 'approve', status: 'prompt', info: `Approve the ${NFT_NAME} contract to burn your ${FT_NAME}` }, async updateStep => {
          const approveReceipt = await requestApproval(library, tokenContractWINE, tokenContractUNIWINE.address, fullAllowance ? ethers.constants.MaxUint256 : amountWei)
          console.log('approveReceipt', approveReceipt)
          updateStep({ hash: approveReceipt.hash, status: 'wait', hasCompletionListener: true })
          const fullApproveResult = await approveReceipt.wait()
          console.log('fullApproveResult', fullApproveResult)
        })
      }

      let receipt
      await process.trackStep({ type: 'claim', status: 'prompt' }, async updateStep => {
        const estimatedGasPrice = await estimateGasPrice(library)
        const estimatedGasLimit = await tokenContractUNIWINE.estimateGas.claimFromFT(account)
        console.debug('claimFromFT gas estimate:', estimateGasPrice, estimatedGasLimit)

        const response = await tokenContractUNIWINE.claimFromFT(account, {
          gasLimit: calculateGasMargin(estimatedGasLimit),
          gasPrice: estimatedGasPrice,
        })
        updateStep({ hash: response.hash, status: 'wait', hasCompletionListener: true })
        receipt = await response.wait()
        console.debug('claimFromFT receipt', receipt)
      })
      process.setSuccessful()
      return receipt
    } finally {
      process.setInactive()
    }
  }, [account, allowanceClaimWINE, fullAllowance, library, tokenContractUNIWINE, tokenContractWINE])

  const flipToShipped = useCallback(async (tokenIDs: Number[], process: Process, { dontFinishProcess = false }) => {
    try {
      if (!library || !tokenContractUNIWINE) throw new Error('web3 is not ready')
      if (!tokenIDs.length) throw new Error('Empty tokenIDs')

      let receipt
      await process.trackStep({ type: 'convert', status: 'prompt' }, async updateStep => {
        const estimatedGasPrice = await estimateGasPrice(library)
        const estimatedGasLimit = await tokenContractUNIWINE.estimateGas.convertToShipped(tokenIDs)
        console.debug('convertToShipped gas estimate:', estimateGasPrice, estimatedGasLimit)

        const response = await tokenContractUNIWINE.convertToShipped(tokenIDs, {
          gasLimit: calculateGasMargin(estimatedGasLimit),
          gasPrice: estimatedGasPrice,
        })
        updateStep({ hash: response.hash, status: 'wait', hasCompletionListener: true })
        receipt = await response.wait()
        console.debug('convertToShipped receipt', receipt)
      })
      if (!dontFinishProcess) { process.setSuccessful() }
      return receipt
    } finally {
      if (!dontFinishProcess) { process.setInactive() }
    }
  }, [library, tokenContractUNIWINE])

  const result = {
    // fxs:
    setSelectedTokenSymbol,
    validateBuy,
    performTrade,
    validateSell,
    flipToShipped,
    claimFromFT,

    // status info:
    ready,
    networkError,
    selectedTokenSymbol,
    dollarPrice: rateWineUsd,
    ethPrice: rateWineEth,

    // inWallet:
    balanceWINE,
    balanceUNIWINE,
    tokensUNIWINE,
    reserveWineTokens: reserveWineTokens,

    // allowance:
    allowanceSwapWINE,
    allowanceSwapSelected,
    allowanceClaimWINE,

    // total in existence:
    totalSupplyWINE, // currently minted FT
    totalSupplyUNIWINE, // issued NFTs (minted as part of claiming process)
    nextUNIWINE, // next NFT (probably)

    // uniswap token objects:
    WINE,
    UNIWINE,
  }
  const changes = filter(Object.keys(result).map(f => result[f] !== changeTracker[f] ? [f, result[f]] : null))
  if (changes.length) console.debug('ethData changes:', fromPairs(changes))
  changeTracker = result
  return result
}

export type EthData = ReturnType<typeof useEthDataCallOnlyFromContextInit>
