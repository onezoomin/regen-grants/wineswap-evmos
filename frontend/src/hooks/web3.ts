import { Web3Provider } from '@ethersproject/providers'
import { ChainId } from '@uniswap/sdk-core'
import { useWeb3React as useWeb3ReactCore } from '@web3-react/core'
import { Web3ReactContextInterface } from '@web3-react/core/dist/types'
import { useEffect, useState } from 'react'
import { isMobile } from 'react-device-detect'
import { injected } from './../network/connectors'
import { NetworkContextName } from './../utils/constants'

// declare let window: any

export function useActiveWeb3React (): Web3ReactContextInterface<Web3Provider> & { chainId?: ChainId } {
  const context = useWeb3ReactCore<Web3Provider>()
  const contextNetwork = useWeb3ReactCore<Web3Provider>(NetworkContextName)
  return context.active ? context : contextNetwork
}

export function useEagerConnect () {
  const { activate, active } = useWeb3ReactCore() // specifically using useWeb3ReactCore because of what this hook does
  const [tried, setTried] = useState(false)

  useEffect(() => {
    injected.isAuthorized().then(isAuthorized => {
      console.log('Trying to eagerly connect to injected provider', { isAuthorized, mobileEager: isMobile && !!window.ethereum })
      if (isAuthorized) {
        activate(injected, undefined, true).catch(() => {
          setTried(true)
        })
      } else {
        // TODO: broken bc. MM - https://github.com/MetaMask/metamask-extension/issues/11231
        // if (isMobile && window.ethereum) { // not sure why there's an exception here, but I guess it's for mobile apps that have a provider available
        //   // console.log('manual', injected.activate())
        //   console.log('activate prom', activate(injected, undefined, true).then(
        //     () => {
        //       console.log('Activation done')
        //       setTried(true)
        //     }, err => {
        //       console.log('Activation failed', err)
        //       setTried(true)
        //     }))
        // } else {
        setTried(true)
        // }
      }
    })
  }, [activate]) // intentionally only running on mount (make sure it's only mounted once :))

  // if the connection worked, wait until we get confirmation of that to flip the flag
  useEffect(() => {
    if (active) {
      setTried(true)
    }
  }, [active])

  return tried
}

/**
 * Use for network and injected - logs user in
 * and out after checking what network theyre on
 */
export function useInactiveListener (suppress = false) {
  const { active, error, activate } = useWeb3ReactCore() // specifically using useWeb3React because of what this hook does

  useEffect(() => {
    const { ethereum } = window

    if (ethereum?.on && !active && (error == null) && !suppress) {
      const handleChainChanged = async () => {
        try {
          console.log('Activating after chain changed')
          const result = await activate(injected, undefined, true)
          console.log('Successfully to activate after chain changed', result)
        } catch (error) {
          console.error('Failed to activate after chain changed', error)
        }
      }

      const handleAccountsChanged = async (accounts: string[]) => {
        console.log('Accounts changed', accounts)
        if (accounts.length > 0) {
          try {
            console.log('Activating after accounts changed')
            const result = await activate(injected, undefined, true)
            console.log('Successfully to activate after accounts changed', result)
          } catch (error) {
            console.error('Failed to activate after accounts changed', error)
          }
        }
      }

      // eslint-disable-next-line @typescript-eslint/no-misused-promises
      ethereum.on('chainChanged', handleChainChanged)
      // eslint-disable-next-line @typescript-eslint/no-misused-promises
      ethereum.on('accountsChanged', handleAccountsChanged)

      return () => {
        if (ethereum.removeListener) {
          // eslint-disable-next-line @typescript-eslint/no-misused-promises
          ethereum.removeListener('chainChanged', handleChainChanged)
          // eslint-disable-next-line @typescript-eslint/no-misused-promises
          ethereum.removeListener('accountsChanged', handleAccountsChanged)
        }
      }
    }
    return undefined
  }, [active, error, suppress, activate])
}
