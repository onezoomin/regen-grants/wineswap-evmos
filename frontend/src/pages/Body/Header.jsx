
import { Link } from 'react-router-dom'
import { ConnectButton } from '../../components/Buttons'
import { useDetectNetwork } from '../../hooks/main'
import WINESWAPimage from '../../img/wineswap_lettering.png'
import { FullWidthBarClasses } from './index'

export function Header ({ setShowConnect }) {
  const { networkName, networkID, networkError } = useDetectNetwork()
  const HeaderBarClasses = `${FullWidthBarClasses} top-0 left-0 z-30 p-4 items-center bg-black bg-opacity-40 relative`

  return (
    <div id="FullWidthBarHeader" {...{ className: HeaderBarClasses }}>

      <Link to="/" className="w-2/5 max-w-xxs h-fit z-10 mt-1 ml-3 flex">
        <img src={WINESWAPimage} alt="WINESWAP Logo - linking to app home" />
      </Link>

      <div className="flex items-center relative">
        {networkName && networkName === 'homestead'
          ? null
          : (
            <div id="networkName" className="absolute left-0 top-full w-full text-xs text-center">
              <em>{networkName === 'unknown' ? networkID : networkName}</em>
            </div>
            )}
        <ConnectButton {...{ setShowConnect }} />
      </div>

      {!networkError ? null : <div className="absolute left-0 top-full w-full bg-red-500 p-4">{networkError.message}</div>}

    </div>
  )
}
