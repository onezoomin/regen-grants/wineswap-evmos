import { Button, createTheme as createMuiTheme, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, MuiThemeProvider } from '@material-ui/core'
import { lightBlue } from '@material-ui/core/colors'
import { SnackbarProvider } from 'notistack'
import { Dispatch, ReactNode, SetStateAction, useContext, createContext, useState } from 'react'
import { formatError } from '../../utils/error'
import { rIF } from '../../utils/react-utils'
import { isWalletCancelError } from '../../utils/web3-utils'

interface ErrorDialog {
  title?: string
  message?: ReactNode
  error?: Error|object
}

export interface UIContextType {
  errorDialog: (title: string, error: Error|object, additionalContent?: ReactNode) => void
}
export const UIContext = createContext<UIContextType>({ errorDialog: () => { throw new Error('No Context') } })

export default function UIProvider ({ children }) {
  const [errorDialog, setErrorDialog] = useState<ErrorDialog|null>(null /* for test: { title: 'Test', message: 'Hello there' } */)

  const theme = createMuiTheme({
    palette: {
      type: 'dark',
      // dafuq ? https://github.com/mui-org/material-ui/issues/18776
      primary: lightBlue,
    },
  })

  const handleCloseModal = () => setErrorDialog(null)
  errorDialog && console.log('Showing errorDialog:', errorDialog)
  return <UIContext.Provider value={{ errorDialog: (title, error, additional) => showErrorDialog(setErrorDialog, title, error, additional) }}>
    <MuiThemeProvider theme={theme}>
      <SnackbarProvider maxSnack={3}>

        {children}

        <Dialog
          open={!!errorDialog}
          onClose={handleCloseModal}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">{errorDialog?.title ?? 'Error'}</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              {rIF(errorDialog?.error, <pre>{formatError(errorDialog?.error as object)}</pre>)}
              {rIF(errorDialog?.message && errorDialog?.error, <div className="mb-2"/>)}
              {rIF(errorDialog?.message, errorDialog?.message)}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleCloseModal} color="primary">
              OK
            </Button>
          </DialogActions>
        </Dialog>
      </SnackbarProvider>
    </MuiThemeProvider>
  </UIContext.Provider>
}

export function useUIContext (): UIContextType {
  return useContext(UIContext)
}

function showErrorDialog (setErrorDialog: Dispatch<SetStateAction<ErrorDialog|null>>, title: string, error: Error|object, additionalContent?: ReactNode) {
  console.error(title, error)
  if (!isWalletCancelError(error)) {
    setErrorDialog({
      title: title,
      error,
      message: additionalContent,
    })
  }
}
