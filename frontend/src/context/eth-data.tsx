import { createContext, useContext } from 'react'
import { EthData, useEthDataCallOnlyFromContextInit } from '../hooks/eth-data'
import { Process, useProcess } from '../hooks/process'

export interface EthContextType {
  ethData: EthData
  process: Process
}

export const EthContext = createContext<EthContextType|null>(null)

export default function EthProvider ({ children }) {
  const process = useProcess()
  const ethData = useEthDataCallOnlyFromContextInit()

  return <EthContext.Provider value={{ ethData, process }}>{children}</EthContext.Provider>
}

export function useEthContext (): EthContextType {
  const context = useContext(EthContext)
  if (context === null) throw new Error('Context is null >:(')
  return context
}
