import { log } from './utils'

export const addOrderNotion = async (request) => {
  // extract params
  const {
    shippingAddress = '123 overther',
    signature = 'ASC123',
    account = '0xEEE...DDD',
    nftList = [1, 2, 3],
    email = 'foo@bar.com',
  } = request.params

  // Notion config
  const config = {
    NOTION_KEY: '',
    DB_ID: '',
  }
  const {NOTION_KEY, DB_ID} = config 

  const notionURL = 'https://api.notion.com/v1/pages/'
  log(`addOrderNotion to db: ${DB_ID}`)

  // Notion Object
  const baseShipmentObj = {
    parent: { type: 'database_id', database_id: DB_ID },
    properties: {
      ID: {
        type: 'title',
        title: [{ type: 'text', text: { content: `${nftList.length} Bottles -> ${email}` } }],
      },
      NFTs: {
        type: 'rich_text',
        rich_text: [{ type: 'text', text: { content: JSON.stringify(nftList) } }],
      },
      Wallet: {
        type: 'rich_text',
        rich_text: [{ type: 'text', text: { content: account } }],
      },
      Signature: {
        type: 'rich_text',
        rich_text: [{ type: 'text', text: { content: signature } }],
      },
      SignedAddress: {
        type: 'rich_text',
        rich_text: [{ type: 'text', text: { content: shippingAddress } }],
      },
      Ordered: {
        type: 'date',
        date: { start: (new Date()).toISOString() },
      },
      Status: {
        type: 'multi_select',
        multi_select: [{ name: 'Requested' }],
      },
      // mo ralisId: {
      //   type: 'rich_text',
      //   rich_text: [{ type: 'text', text: { content: mo ralisId } }],
      // },
    },
  }
  log(JSON.stringify(baseShipmentObj, null, 2))
  let httpResponse; let status = 'success'
  try {
    // httpResponse = await Mo ralis.Cloud.httpRequest({
    //   method: 'POST',
    //   url: notionURL,
    //   followRedirects: true,
    //   headers: {
    //     'Content-Type': 'application/json',
    //     Authorization: `Bearer ${NOTION_KEY}`,
    //     'Notion-Version': '2021-05-13',
    //   },
    //   body: baseShipmentObj,
    // })
    throw new Error('TODO replace mo ralis')
  } catch (error) {
    status = 'error'
    log(9, 'caught', JSON.stringify(error.data, null, 2))
  }

  log(`notion ${status}::\n ${JSON.stringify(httpResponse.data, null, 2)}`)

  return { ...request.params, httpResponse, status }
}
