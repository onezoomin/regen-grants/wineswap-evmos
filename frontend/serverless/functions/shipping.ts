import { Web3Provider } from '@ethersproject/providers'
import { sendToDiscord } from './discord'
import { addOrderNotion } from './notion'
import { log } from './utils'

const DISCORD_SHIPMENTS = 'https://discord.com/api/webhooks/850025983683985488/h-Q9Jjsi17vPP0L9v5LeOpVZAT9QxF71twG_ZIMNTH-1Cz-QrADR90GrmGkM4sZMbubW?wait=true'
const DISCORD_UNVERIFIED_SHIPMENTS = 'https://discord.com/api/webhooks/850307328502005760/_DGH-T5qh6HX-vYHZq5nhbaJMveDNfrbUdbD5F6UMMqVxzxI_AGlVE-ku0vQ2dPVD0-t?wait=true'

export const handleShipping = async (request) => {
  const config = { CHAIN_ID: '0x4' }
  const { CHAIN_ID } = config
  // const web3 = Web3Provider// Moralis.web3ByChain(CHAIN_ID) // mainnet=0x1 goerli=0x3 rinkeby=0x4
  log(`handleShipping on chain: ${CHAIN_ID}`)

  throw new Error('TODO - handleShipping')

  // const {
  //   signedMessage,
  //   shippingAddress = '123 overther',
  //   signature = 'ASC123',
  //   account = '0xEEE...DDD',
  //   nftList = [1, 2],
  //   email = 'foo@bar.com',
  // } = request.params

  // pre-save order in moralis
  // const placedOrder = new (Moralis.Object.extend('PlacedOrder'))()
  // const orderObj = { shippingAddress, signature, account, nftList, email }

  // let ord; const retObj: any = { }
  // try {
  //   ord = await placedOrder.save(orderObj)
  //   log(`New object created with objectId: ${ord.id} `, ord)
  //   retObj.placedOrderId = request.params.moralisId = ord.id
  // } catch (error) {
  //   log(`Failed to create new object, with error code: ${error.message}`)
  // }

  // Check Signature
  // const verifyResult = await web3.eth.accounts.recover(signedMessage, signature)
  // const isMatching = verifyResult === account
  // log(`${isMatching} Verification result: ${verifyResult}`)

  // // TODO enforce ownership of this specific nft
  // const isTheirsToShip = true
  // const channelHookURL = isMatching && isTheirsToShip ? DISCORD_SHIPMENTS : DISCORD_UNVERIFIED_SHIPMENTS

  // try {
  //   if (isMatching && isTheirsToShip) retObj.notion = await addOrderNotion(request)
  // } finally {
  //   retObj.discord = await sendToDiscord(channelHookURL, `request:\n${signedMessage}\nsignature:\n${signature}\naccount:\n${account}`)
  // }
  // return retObj
}
