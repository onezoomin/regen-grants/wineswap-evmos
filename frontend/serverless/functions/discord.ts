import { createOrUpdateDiscordUser } from './data/discord'
import { http, log } from './utils'

const REDIRECT_URLS = [
  'http://localhost:3000/#/oauth-callback/discord',
  'https://dev.swp.wine/#/oauth-callback/discord',
]

async function botAuth () {
  const config = {
    CLIENT_ID:'',
    CLIENT_SECRET:'',
    DISCORD_TOKEN:'',
    DISCORD_GUILD:'',
  }
  const clientId = config.CLIENT_ID
  const clientSecret = config.CLIENT_SECRET
  const botToken = config.DISCORD_TOKEN
  const wineSwapGuild = config.DISCORD_GUILD

  return { botToken, clientId, clientSecret, wineSwapGuild }
}

export const discordAuth = async (request) => {
  // log(`[discordAuth] request ${JSON.stringify(request, null, 2)}`)
  if (!REDIRECT_URLS.includes(request.params.redirectUri)) {
    throw new Error('Invalid redirectUrl')
  }
  const client = await botAuth()

  let authResponse
  try {
    authResponse = await http({
      url: 'https://discord.com/api/oauth2/token',
      contentType: 'application/x-www-form-urlencoded',
      body: {
        client_id: client.clientId,
        client_secret: client.clientSecret,
        grant_type: 'authorization_code',
        code: request.params.authCode,
        redirect_uri: request.params.redirectUri,
      },
    })
    log(`discordAuth res: ${JSON.stringify(authResponse.data, null, 2)}`)
  } catch (error) {
    log(9, '[discordAuth] oauth2 token request failed', authResponse.data || authResponse)
    return { status: 'error' }
  }

  const authData = authResponse?.data
  if (!authData?.access_token) {
    log(9, `[discordAuth] no accesstoken: ${JSON.stringify(authResponse.data || authResponse, null, 2)}`)
    return { status: 'error' }
  } else {
    log('[discordAuth] requesting user info')
    const userResult = await http({
      method: 'GET',
      url: 'https://discord.com/api/users/@me',
      auth: `Bearer ${authData.access_token}`,
    })
    log('[discordAuth] user:', userResult.data)
    const user = await createOrUpdateDiscordUser(authData, userResult.data, request.params.ethAddress, request.params.moralisId)

    log(`[discordAuth] joining guild: ${client.wineSwapGuild}`)
    const joinResult = await http({
      method: 'PUT',
      url: `https://discord.com/api/guilds/${client.wineSwapGuild}/members/${userResult.data.id}`,
      auth: `Bot ${client.botToken}`,
      body: {
        access_token: authData.access_token,
      },
    })
    log('[discord.botAuth] join result', joinResult.status)

    log('[discordAuth] creating Channel')
    const channelResult = await http({
      url: 'https://discord.com/api/users/@me/channels',
      auth: `Bot ${client.botToken}`,
      body: {
        recipient_id: userResult.data.id,
      },
    })
    log('[discord.botAuth] channel create result', channelResult)
    log('[discordAuth] sending DM')
    const dmResult = await http({
      url: `https://discord.com/api/channels/${channelResult.data.id}/messages`,
      auth: `Bot ${client.botToken}`,
      body: {
        content: `Welcome, ${request.params.ethAddress}!`,
      },
    })
    log('[discord.botAuth] DM result', dmResult)

    user.set('joined', true)
    user.save()

    return { status: 'success' }
  }
}

export const sendToDiscord = async (channelHookURL, content) => {
  // https://discord.com/developers/docs/resources/webhook#execute-webhook
  let httpResponse
  try {
    // httpResponse = await Mo ralis.Cloud.httpRequest({
    //   method: 'POST',
    //   url: channelHookURL,
    //   body: {
    //     flags: 1 << 2,
    //     content,
    //   },
    // })
    throw new Error('TODO replace Mo ralis')
  } catch (error) {
    log(`[sendToDiscord] error ${JSON.stringify(httpResponse, null, 2)}`)
    return { status: 'error' }
  }
  log('[sendToDiscord] success')
  // logger.info(JSON.stringify(httpResponse)) return { httpResponse, status }
}
