import { defineConfig } from 'windicss/helpers'
import formsPlugin from 'windicss/plugin/forms'

export default defineConfig({
  purge: {
    content: ['./src/**/*.{js,jsx,ts,tsx}', './index.html'],
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
      // 'sans': ['ui-sans-serif', 'system-ui', ...],
      // 'serif': ['ui-serif', 'Georgia', ...],
      // 'mono': ['ui-monospace', 'SFMono-Regular', ...],
      // 'display': ['Oswald', ...],
      sans: ['Montserrat', 'ui-sans-serif', 'system-ui'],
    },
    extend: {
      backgroundImage: () => ({
        grapes: "url('/src/img/grapes.jpg')",
        'wine-table': "url('/src/img/wineLandscape01crop.jpeg')",
        vinyard: "url('/src/img/vinyardPTcrop.jpeg')", // /Users/gotjosh/Dev/uniwine/src/img/vinyardPTcrop.jpeg
      //  'footer-texture': "url('/img/footer-texture.png')",
      }),
      screens: {
        xxs: '360px',
        xs: '480px',
        // ...defaultTheme.screens,
      },
      backgroundPosition: {
        'bottom-oc-10': 'bottom right 60%',
      },
      backgroundColor: {
        'grey-850': 'rgba(35, 35, 35, 1)',
      },
      zIndex: {
        '-1': '-1',
        '-2': '-2',
      },
      maxWidth: {
        2: '0.5rem',
        3: '0.75rem',
        22: '5.5rem',
        '200px': '200px',
        xxs: '12rem',
        'max-content': 'max-content',
        fit: 'fit-content',
      },
      minHeight: {
        'max-content': 'max-content',
        fit: 'fit-content',
        'real-screen': 'calc(var(--vh) * 100)',
        0: '0',
        '1/4': '25%',
        '1/2': '50%',
        '3/4': '75%',
        full: '100%',
        210: '210px',
      },
      height: {
        'max-content': 'max-content',
        fit: 'fit-content',
        em: '1em',
        'real-screen': 'calc(var(--vh) * 100)',
      },
      maxHeight: {
        'max-content': 'max-content',
        fit: 'fit-content',
        'real-screen': 'calc(var(--vh) * 100)',
      },
      scale: {
        102: '1.02',
      },
      animation: {
        'bounce-d100': 'bounce 1s infinite 100ms',
        'bounce-d200': 'bounce 1s infinite 200ms',
        'pulse-d100': 'pulse 2s cubic-bezier(0.4, 0, 0.6, 1) infinite 100ms',
        'pulse-d200': 'pulse 2s cubic-bezier(0.4, 0, 0.6, 1) infinite 200ms',
      },
    },
  },
  variants: {
    extend: [],
  },
  plugins: [
    formsPlugin,
    // plugin(function ({ addUtilities }) {
    //   const newUtilities = {
    //     '.animation-delay-100': {
    //       animationDelay: '100ms',
    //     },
    //   }

    //   addUtilities(newUtilities, ['responsive', 'hover', 'motion-safe'])
    // }),
  ],
})
